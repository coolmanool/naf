from django.utils.translation import ugettext_lazy as _
from django import forms


class ProjectForm(forms.Form):
    title = forms.CharField(label=u'Название')
    title_eng = forms.CharField(label=u'Title', required=False)
    text = forms.CharField(widget=forms.Textarea, label=u'Описание', required=False)
    text_eng = forms.CharField(widget=forms.Textarea, label=u'Description', required=False)
    photos = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}),
                             required=False,
                             label=u'Фотографии')
    video = forms.URLField(label=u'Ссылка на видео с Youtube', required=False)
    date = forms.DateField(widget=forms.DateInput(attrs={'placeholder': 'dd.mm.yyyy'}),
                           label=u'Дата',
                           input_formats=['%d.%m.%y', '%d.%m.%Y'])

    def is_valid(self):
        valid = super(ProjectForm, self).is_valid()

        if not valid:
            return valid

        exts = ('jpg', 'jpeg', 'bmp', 'png', 'gif')
        valid_flag = True
        if self.files:
            files = self.files.getlist('photos')

            for f in files:
                ext = f.name.split('.')[-1].lower()
                if ext not in exts:
                    self._errors['photos'] = 'Invalid format file: {}'.format(f.name)
                    valid_flag = False

        return valid_flag


class DeleteObjectsForm(forms.Form):
    obj_ids = forms.CharField()


class MakePhotoMainForm(forms.Form):
    photo_new_id = forms.CharField()
    photo_old_id = forms.CharField(required=False)


class MessageForm(forms.Form):
    name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': _('Ваше имя')}))
    theme = forms.CharField(widget=forms.TextInput(attrs={'placeholder': _('Тема')}))
    message = forms.CharField(widget=forms.Textarea(attrs={'placeholder': _('Сообщение')}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'placeholder': _('Ваш e-mail')}))
    msg_type = forms.CharField()
