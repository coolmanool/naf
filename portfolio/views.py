from sorl.thumbnail import get_thumbnail
from django.utils import translation
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail, BadHeaderError
from django.shortcuts import render, redirect, get_object_or_404
from .forms import ProjectForm, DeleteObjectsForm, MessageForm, MakePhotoMainForm
from .models import Project, Photo
import json


def portfolio(request):
    if request.method == 'POST':
        form = MessageForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            msg = form.cleaned_data['message']
            message = 'Сообщение отправлено с сайта.\nИмя: {}\nE-mail: {}\n\n{}'.format(name, email, msg)
            message_reminder = 'Только что с адреса {} было отправлено сообщение на сайте агентства NAF ' \
                               'со следующим содержанием:\n{}\n\n' \
                               'Если Вы этого не делали, пожалуйста, сообщите об этом ' \
                               'ответом на это письмо.'.format(email, msg)

            try:
                send_mail(
                    form.cleaned_data['theme'],
                    message,
                    'nafserver@yandex.ru',
                    # change emails
                    ['coop@nafmedia.ru' if form.cleaned_data['msg_type'] == 'coop' else 'sales@nafmedia.ru']
                )

                send_mail(
                    u'Вы отправили сообщение с сайта агенства NAF',
                    message_reminder,
                    'nafserver@yandex.ru',
                    [email]
                )
                return JsonResponse({'response': 'ok'})
            except BadHeaderError:
                return HttpResponse('Invalid header found.')
        else:
            return JsonResponse({'response': 'error',
                                 'errors': form.errors.as_json()})
    form = MessageForm()

    photos = Photo.objects.filter(main_photo_flag=True).select_related('project')
    projects = []
    for p in photos:
        th = get_thumbnail(p.photo, '445x300', crop='center', quality=99)
        projects.append({'title': p.project.title if translation.get_language() == 'ru' else p.project.title_eng,
                         'id': p.project.id,
                         'th_url': th.url,
                         'date': p.project.date})

    projects = sorted(projects, key=lambda d: d['date'], reverse=True)
    return render(request,
                  'portfolio/main.html',
                  {'form': form,
                   'projects': projects})


def ajax_project(request):
    if request.method == 'GET':
        current_lang = translation.get_language()
        proj = Project.objects.get(id=request.GET.get('pk'))
        videos = json.loads(proj.videos)
        photos = []
        for photo in Photo.objects.filter(project=proj):
            photos.append(photo.photo.url)
        return JsonResponse({'title': proj.title if current_lang == 'ru' else proj.title_eng,
                             'text': proj.text if current_lang == 'ru' else proj.text_eng,
                             'photos': photos,
                             'videos': videos})
    else:
        pass


@login_required()
def draft(request):
    return render(request, 'draft.html', {})


@login_required()
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST, request.FILES)
        if form.is_valid():
            try:
                proj = Project.objects.create(title=form.cleaned_data['title'],
                                              text=form.cleaned_data['text'],
                                              videos=form.cleaned_data['video'],
                                              title_eng=form.cleaned_data['title_eng'],
                                              text_eng=form.cleaned_data['text_eng'],
                                              date=form.cleaned_data['date'])
                if form.files:
                    for f in form.files.getlist('photos'):
                        Photo.objects.create(project=proj,
                                             photo=f)
                return redirect('edit_project', pk=proj.pk)
            except Exception as e:
                print(e)
        else:
            return render(request,
                          'admin/create_project.html',
                          {'form': form})
    else:
        form = ProjectForm()
    return render(request,
                  'admin/create_project.html',
                  {'form': form})


@login_required()
def edit_project(request, pk):
    proj = get_object_or_404(Project, pk=pk)
    photos = Photo.objects.filter(project=proj).order_by('id')
    if request.method == 'POST':
        if 'obj_ids' in request.POST:  # delete photos
            photos_form = DeleteObjectsForm(request.POST)
            if photos_form.is_valid():
                photo_ids = photos_form.cleaned_data['obj_ids'].split(',')
                for i in photo_ids:
                    Photo.objects.filter(id=i).delete()
        elif 'photo_new_id' in request.POST:  # change main photo
            photo_form = MakePhotoMainForm(request.POST)
            if photo_form.is_valid():
                if 'photo_old_id' in request.POST:
                    photo_old_id = int(photo_form.cleaned_data['photo_old_id'])
                    photos.filter(id=photo_old_id).update(main_photo_flag=False)
                photo_new_id = int(photo_form.cleaned_data['photo_new_id'])
                photos.filter(id=photo_new_id).update(main_photo_flag=True)
        else:  # change project info
            project_form = ProjectForm(request.POST, request.FILES)
            if project_form.is_valid():
                try:
                    proj.title = project_form.cleaned_data['title']
                    proj.text = project_form.cleaned_data['text']
                    proj.videos = project_form.cleaned_data['video']
                    proj.title_eng = project_form.cleaned_data['title_eng']
                    proj.text_eng = project_form.cleaned_data['text_eng']
                    proj.date = project_form.cleaned_data['date']
                    proj.save()
                    if project_form.cleaned_data['photos']:
                        for f in project_form.files.getlist('photos'):
                            Photo.objects.create(project=proj,
                                                 photo=f)
                    return redirect('edit_project', pk=pk)
                except Exception as e:
                    print(e)
            else:
                return render(request,
                              'admin/edit_project.html',
                              {'form': project_form,
                               'proj': proj,
                               'photos': photos})
    video = json.loads(proj.videos)
    project_form = ProjectForm(initial={'title': proj.title,
                                        'text': proj.text,
                                        'title_eng': proj.title_eng,
                                        'text_eng': proj.text_eng,
                                        'date': proj.date,
                                        'video': video[0] if video else None})
    return render(request,
                  'admin/edit_project.html',
                  {'form': project_form,
                   'proj': proj,
                   'photos': photos})


@login_required()
def projects(request):
    if request.method == 'POST':
        form = DeleteObjectsForm(request.POST)
        if form.is_valid():
            project_ids = form.cleaned_data['obj_ids'].split(',')
            for i in project_ids:
                Project.objects.filter(id=i).delete()
    projects = Project.objects.all().order_by('-date')
    return render(request,
                  'admin/projects.html',
                  {'projects': projects})


def robots(request):
    return render(request, 'seo/robots.txt', content_type='text/plain')


def sitemap(request):
    return render(request, 'seo/sitemap.xml', content_type='text/xml')


def yamail(request):
    return render(request, 'admin/yamail.html', content_type='text/plain')
