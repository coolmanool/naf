module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            dist: {
                files: {
                    'style/build/portfolio.styl': ['style/styl-common/*.styl', 'style/styl-portfolio/*.styl', '!style/styl-portfolio/slider.styl'],
                    'style/build/admin.styl': ['style/styl-common/*.styl', 'style/styl-admin/*.styl'],
                    'scripts/build/script-portfolio.js': ['scripts/src-common/csrf.js', 'scripts/src-portfolio/*.js', 'scripts/src-portfolio/load.js', '!scripts/scr-portfolio/slider.js'],
                    'scripts/build/script-admin.js': ['scripts/src-common/csrf.js', 'scripts/src-admin/*.js', 'scripts/src-admin/load.js']
                }
            }
        },
        stylus: {
            compile: {
                files: [{
                    expand: true,
                    cwd: 'style/build',
                    src: 'portfolio.styl',
                    dest: 'style/build',
                    ext: '.css'
                }, {
                    expand: true,
                    cwd: 'style/build',
                    src: 'admin.styl',
                    dest: 'style/build',
                    ext: '.css'
                }]
            }
        },
        uglify: {
            scripts: {
                files: {
                    'scripts/build/script-admin.min.js': 'scripts/build/script-admin.js',
                    'scripts/build/script-portfolio.min.js': 'scripts/build/script-portfolio.js'
                }
            }
        },
        cssmin: {
            styles: {
                files: {
                    'style/build/admin.min.css': 'style/build/admin.css',
                    'style/build/portfolio.min.css': 'style/build/portfolio.css'
                }
            }
        },
        watch: {
            css: {
                files: 'style/styl-*/*.styl',
                tasks: ['concat', 'stylus', 'cssmin'],
                options: {
                    spawn: false
                }
            },
            scripts: {
                files: 'scripts/src-*/*.js',
                tasks: ['concat', 'uglify'],
                options: {
                    spawn: false
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-stylus');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['concat', 'stylus', 'uglify', 'cssmin', 'watch']);
};