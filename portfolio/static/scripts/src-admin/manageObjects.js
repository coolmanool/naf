var getIds = function getIds(objs) {
    var objectIds = '';
    objs.each(function() {
       objectIds += $(this).val() + ',';
    });
    return objectIds.slice(0, -1);
};

var confirmDelete = function confirmDelete(objs) {
    var ids = getIds(objs);
    if (ids != '') {
        if (confirm('Please confirm delete')) {
            ajaxDelete(ids);
        }
    }
};

var ajaxDelete = function ajaxDelete(ids) {
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        }
    });

    $.ajax({
        type: "POST",
        data: {obj_ids: ids},
        success: function() {
            window.location.reload();
        },
        error: function() {
            console.log('ERROR');
        }
    });
};

var ajaxMakePhotoMain = function ajaxMakePhotoMain() {
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        }
    });

    $.ajax({
        type: "POST",
        data: {photo_new_id: $(this).attr('data-photo-id'),
            photo_old_id: $('.photo[data-photo-main="True"]').attr('data-photo-id')},
        success: function(){
            window.location.reload();
        },
        error: function(){
            console.log('ERROR');
        }
    });
};