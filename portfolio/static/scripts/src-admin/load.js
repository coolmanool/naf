$(function() {
    $('.js-btn-photos-delete').on('click', function() {
        confirmDelete($('.photo .check input:checked'))
    });
    $('.js-btn-projects-delete').on('click', function() {
        confirmDelete($('.project .check input:checked'))
    });

    $('.js-make-main').on('click', ajaxMakePhotoMain);
});
