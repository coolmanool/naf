var putDefaultMargins = function putDefaultMargins() {
    $('.project').removeClass('project-second').each(function(index) {
        if (index % 3 == 1) {
            $(this).addClass('project-central');
        }
    });
};

var putHalfMargins = function putHalfMargins() {
    $('.project').removeClass('project-central').each(function(index) {
        if (index % 2 == 1) {
            $(this).addClass('project-second');
        }
    });
};

var removeMargins = function removeMargins() {
    $('.project').removeClass('project-central').removeClass('project-second');
};

var checkWinWidth = function checkWinWidth() {
    var winWidth = $(window).width();
    if (winWidth >= 950) {
        putDefaultMargins();
    } else if(winWidth > 600) {
        putHalfMargins();
    } else {
        removeMargins();
    }
};