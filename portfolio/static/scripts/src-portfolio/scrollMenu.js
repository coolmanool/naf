var checkScroll = function checkScroll(scrollItems, $lastScrollItem, lastScrollItemAttr) {
    var cur = scrollItems.map(function() {
            if ($(this).offset().top <= $(window).scrollTop() + 76) {
                return this;
            }
        }),
        $cur = $(cur[cur.length - 1]),
        curAttr = $cur.attr('data-menu-dest'),
        menuNormalColor = '#3b3b3b',
        menuHoverColor = '#0078c7';

    if (lastScrollItemAttr != curAttr) {
        if ($lastScrollItem) {
            $("[data-menu-src="+lastScrollItemAttr+"]").css('color', menuNormalColor);
        }
        $("[data-menu-src="+curAttr+"]").css('color', menuHoverColor);
        $lastScrollItem = $cur;
        lastScrollItemAttr = curAttr;
    }

    if  ($(window).scrollTop() == $(document).height() - $(window).height()) {
        if ($lastScrollItem) {
            $("[data-menu-src="+lastScrollItemAttr+"]").css('color', menuNormalColor);
        }
        $cur = $("[data-menu-src='js-contacts']");
        $cur.css('color', menuHoverColor);
        $lastScrollItem = $cur;
        lastScrollItemAttr = 'js-contacts';
    }

    return [$lastScrollItem, lastScrollItemAttr]
};

var scrollMenu = function scrollMenu(lastScroll, $lastScrollItem, lastScrollItemAttr, scrollItems) {
    var $this = $(this),
        animateTime = 600,
        easing = "linear",
        $header = $(".js-header"),
        $headerOpacity = $(".js-header-opacity");

    if (lastScroll == 0) {
        $header.css('display', 'block');
        $header.animate(
            {'opacity': '1'},
            animateTime,
            easing
        );

        $headerOpacity.animate(
            {'opacity': '0'},
            animateTime,
            easing,
            function () { $(this).css('display', 'none'); }
        );

        lastScroll = $this.scrollTop();

    } else if (($this.scrollTop() <= 0) && ($('.js-menu-hamburger').css('display') == 'none')) {
        lastScroll = $this.scrollTop();
        $header.animate(
            {'opacity': '0'},
            animateTime,
            easing,
            function() { $(this).css('display', 'none') }
        );

        $headerOpacity.css('display', 'block');
        $headerOpacity.animate(
            {'opacity': '1'},
            animateTime,
            easing
        );
    }

    var lastScrollItemArr = checkScroll(scrollItems, $lastScrollItem, lastScrollItemAttr);
    return [lastScroll, lastScrollItemArr[0], lastScrollItemArr[1]]
};
