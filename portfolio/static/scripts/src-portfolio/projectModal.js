var modalProjectOpen = function modalProjectOpen() {
    var src = $(this).attr('data-project');

    $.ajax({
        type: "GET",
        url: '/ajax-project/',
        data: {pk: src},
        success: function(data) {
            var $modalProject = $('#js-modal-project'),
                title = $modalProject.find('.title span'),
                text = $modalProject.find('.text span'),
                $filmroll = $('#filmroll');
            title.text(data['title']);
            text.text(data['text']);

            if ($filmroll.attr('data-is-slicked') === '1') {
                $filmroll.slick('unslick');
            }

            $filmroll.empty();
            $.each(data['videos'], function(index, item) {
                var divElem = $(document.createElement('div')),
                    iframeElem = $(document.createElement('iframe')).attr({'src': item, 'frameborder':'0'});
                $filmroll.append(divElem.append(iframeElem));
            });
            $.each(data['photos'], function(index, item) {
                var divElem = $(document.createElement('div')),
                    //hrefElem = $(document.createElement('a')),
                    imgElem = $(document.createElement('img')).attr('src', item);
                $filmroll.append(divElem.append(imgElem));
            });
            $modalProject.css('display', 'block');

            $filmroll.slick({
                centerMode: true,
                dots: true,
                infinite: true,
                nextArrow: '<button type="button" class="slick-next"><i class="material-icons">chevron_right</i></button>',
                prevArrow: '<button type="button" class="slick-prev"><i class="material-icons">chevron_left</i></button>',
                speed: 300,
                slidesToShow: 1,
                variableWidth: true
            });
            $filmroll.attr('data-is-slicked', '1')
        },
        error: function() {
            console.log('ERROR');
        }
    });
};