var scroll = function scroll() {
    var elementClick = $(this).attr("data-menu-src"),
        destination = $("[data-menu-dest="+elementClick+"]").offset().top,
        headerHeight = $(".header-normal").height();
    $("html:not(:animated),body:not(:animated)").animate({scrollTop: destination - headerHeight}, 800);
};