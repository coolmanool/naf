var modalClose = function modalClose() {
    $(this).closest('.js-modal').css('display', 'none');
};

var modalCloseFromFade = function modalCloseFromFade(event) {
    $(event.target).closest('.js-modal').css('display', 'none');
};

var modalCloseHover = function modalCloseHover(event) {
    console.log($(event.target).closest('.js-close i'));
    $(event.target).find('.js-close i').css('color', 'white');
};

var modalCloseUnhover = function modalCloseUnhover(event) {
    $(event.target).find('.js-close i').css('color', '#d0d0d2');
};

var stopModalPropagation = function stopModalPropagation(e) {
    e.stopPropagation();
};

var modalOpen = function modalOpen() {
    var src = $(this).attr('data-modal-src');
    $("[data-modal-dest=" + src + "]").css('display', 'block');
};

var modalMsgOpen = function modalMsgOpen() {
    var $msgModal = $('#js-msg'),
        $btn = $msgModal.find('.button'),
        $txtarea = $msgModal.find('.theme input'),
        dataMsg = $(this).attr('data-msg');
    if (dataMsg == 'js-coop') {
        $txtarea.val('Предложение о сотрудничестве');
        $btn.attr('data-msg', 'coop');
    } else if (dataMsg == 'js-sales') {
        $txtarea.val('Предложение по продажам');
        $btn.attr('data-msg', 'sales');
    } else {
        return
    }
    $('.js-send-msg-success span').empty();
    $('.js-send-msg-error span').empty();
    $('.errors').empty();
    $msgModal.css('display', 'block');
};