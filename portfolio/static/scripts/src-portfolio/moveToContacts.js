var moveToContacts = function moveToContacts() {
    var headerHeight = $(".header-normal").height(),
        $contactTitle = $(".contact__title");
    $(this).closest('.modal').css('display', 'none');
    $("html:not(:animated),body:not(:animated)").animate({scrollTop: $("[data-menu-dest='js-contacts']").offset().top - headerHeight}, 800);
    $contactTitle.css("color", "#87cefa");
    setTimeout(function() {
        $contactTitle.css("color", "white");
    }, 2000)
};