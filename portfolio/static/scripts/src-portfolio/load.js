$(function() {
    var $window = $(window),
        lastScroll = 0,
        $lastScrollItem = null,
        lastScrollItemAttr = null,
        scrollItems = $("[data-menu-dest^='js']");

    checkWinWidth();

    $window.on('resize', checkWinWidth);
    $window.on('scroll', function() {
        var tmp = scrollMenu(lastScroll, $lastScrollItem, lastScrollItemAttr, scrollItems);
        lastScroll = tmp[0];
        $lastScrollItem = tmp[1];
        lastScrollItemAttr = tmp[2];
    });

    $('.js-modal .js-close').on('click', modalClose);
    $('.js-modal-fade').on('click', modalCloseFromFade);
    $('.modal .content').on('click', stopModalPropagation);
    $('.js-modal-fade').on('mouseover', modalCloseHover);
    $('.modal .content').on('mouseover', stopModalPropagation);
    $('.js-modal-fade').on('mouseout', modalCloseUnhover);
    $('.modal .content').on('mouseout', stopModalPropagation);
    $('[data-modal-src^="js-"]').on('click', modalOpen);

    $('[data-msg^="js-"]').on('click', modalMsgOpen);

    $('.js-hamburger').on('click', hamburgerClick);
    $('.js-hamburger-opacity').on('click', hamburgerOpacityClick);

    $('.js-move-to-contacts').on('click', moveToContacts);

    $('.js-project').on('click', modalProjectOpen);

    $("[data-menu-src^='js-']").on('click', scroll);

    $('.js-send-msg').on('click', sendMsg);

    if ($window.scrollTop() != 0) {
        $(".js-header").css('display', 'block');
        $(".js-header-opacity").css('display', 'none');
    }

    var lastScrollItemArr = checkScroll(scrollItems, $lastScrollItem, lastScrollItemAttr);
    $lastScrollItem = lastScrollItemArr[0];
    lastScrollItemAttr = lastScrollItemArr[1];

    var $player = $('#ytplayer');
    var $loading = $('.loading');

    $player.YTPlayer();

    $player.on('YTPReady', function(e) {
        //
    });

    $player.on('YTPBuffering', function(e) {
       $loading.css('display', 'block');
    });
});