var hamburgerClick = function hamburgerClick() {
    var $this = $(this),
        $menuHamburger = $('.js-menu-hamburger'),
        menuNormalColor = '#3b3b3b',
        menuHoverColor = '#0078c7';
    if ($menuHamburger.css('display') != 'none') {
        $menuHamburger.css('display', 'none');
        $this.css('color', menuNormalColor);
        if ($(window).scrollTop() == 0) {
            $('.js-header-opacity').css({'display': 'block', 'opacity': '1'});
            $('.js-header').css('display', 'none');
        }
    } else {
        $menuHamburger.css('display', 'block');
        $this.css('color', menuHoverColor);
    }
};

var hamburgerOpacityClick = function hamburgerOpacityClick() {
    $('.js-header-opacity').css('display', 'none');
    $('.js-header').css({'display': 'block', 'opacity': '1'});
    $('.js-menu-hamburger').css('display', 'block');
    $('.js-hamburger').css('color', '#0078c7');
    //$('.js-hamburger-opacity').css('color', menuNormalColor);
};