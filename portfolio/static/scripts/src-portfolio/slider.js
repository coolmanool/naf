$(function(){
    var $sliderbox =  $('.slider-box'),
        $slides = $('.slide'),
        $thumbs = $('.slider__thumbs li'),
        slideswidth = $slides.outerWidth(true);

    function getMeTransFromIndex(that) {
        return $(that).index() === 0 ? 'left' : 'right';
    }

    window.makeMoveSlider = function makeMoveSlider(data) {
        var dfd = $.Deferred(),
            params = null;

        if (data === 'left') {
            params = {
                direction: '+=',
                callback: function(){
                    dfd.resolve();
                    $('.slider-box').addClass('_show');
                },
                beforeAnimate: function() {
                    $slides.last().prependTo($sliderbox);
                    $sliderbox.css('left', -734*2);
                    $slides = $('.slide');
                }
            };
        } else {
            params = {
                direction: '-=',
                callback: function(){
                    $slides.first().appendTo($sliderbox);
                    $slides = $('.slide');
                    $sliderbox.css('left',-693);
                    dfd.resolve();
                    $('.slider-box').addClass('_show');
                }
            };
        }

        sliderspin(params);
        return dfd.promise();
    };

    function makeSwitchBubbles(data) {
        var curPos = $thumbs.index($thumbs.filter('._act')),
            iterator = (function() {
                return data === 'left' ? -1 : +1;
            }()),
            nextIndex = (function() {
                var value = null;

                if (iterator > 0 && curPos < $thumbs.length - 1 || iterator < 0 && curPos > 0) {
                    value = curPos + iterator;
                } else if(iterator > 0) {
                    value = 0;
                } else if (iterator < 0) {
                    value = $thumbs.length - 1;
                }

                return value;
            }());

        $thumbs.removeClass('_act').eq(nextIndex).addClass('_act');
    }

    window.decoratorMoveSlider = function decoratorMoveSlider(data) {
        var direction = data < 0 ? 'left' : 'right',
            counter = Math.abs(data);

        $slides.trigger('mouseleave');
        (function loop(counter) {
            makeSwitchBubbles(direction);
            makeMoveSlider(direction).done(function() {
                if (counter -= 1 >= 0) {
                    loop(counter)
                } else {
                    $slides.trigger('mouseenter');
                }
            });
        }(counter));

    };

    function sliderspin(params){
        if ('beforeAnimate' in params) {
            params.beforeAnimate();
        }
        $('.slider-box').removeClass('_show');
        $sliderbox.animate({left: params.direction + slideswidth}, 1000, 'linear', params.callback)
    }

    $thumbs.click(function(){
        if ($sliderbox.is(':animated')) return;

        var curBubbleIndex = $(this).index(),
            actBubbleIndex = $thumbs.index($thumbs.filter('._act'));

        if (actBubbleIndex === curBubbleIndex) {
            return false;
        }

        $slides.trigger('mouseleave');
        decoratorMoveSlider(curBubbleIndex - actBubbleIndex);
    });

    $slides.on({
        click: function(){
            if($sliderbox.is(':animated')){
                return false
            }
            $slides.trigger('mouseleave');

            clearInterval(interval);

            var data = getMeTransFromIndex(this);
            makeMoveSlider(data).done(function() {
                $slides.trigger('mouseenter');
                interval =  setInterval(function() {
                        makeMoveSlider();
                        makeSwitchBubbles();
                    }, 5500
                );
            });
            makeSwitchBubbles(data);
        },
        mouseenter: function() {
            var that = this;

            setTimeout(function() {
                if ($sliderbox.is(':animated')) return false;

                if (/0|2/.test($(that).index())) {
                    $(that).addClass('_hover')
                }
            }, 0)
        },
        mouseleave: function() {
            $(this).removeClass('_hover')
        }
    });
    var interval = setInterval(function() {
            makeMoveSlider();
            makeSwitchBubbles()

        }, 100000
    );

    $thumbs.on({
        'mouseenter':function(){
            clearInterval(interval);
        },
        'mouseleave':function(){
            interval =  setInterval(function() {
                    makeMoveSlider();
                    makeSwitchBubbles();
                }, 5500
            );
        }
    })
})
