var checkMsgFields = function checkMsgFields($btn) {
    var form = $btn.closest('form'),
        inputFields = form.find('input'),
        flag = true;
    inputFields.push(form.find('textarea')[0]);
    inputFields.each(function() {
        if ($(this).val() === '') {
            $('.js-send-msg-error span').text('Все поля формы должны быть заполнены!');
            flag = false;
        }
    });
    return flag
};

var sendMsg = function sendMsg() {
    if (checkMsgFields($(this))) {
        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
            }
        });

        var arr = $(this).closest('form').serializeArray(),
            dict = {};
        arr.push({name: 'msg_type', value: $(this).attr('data-msg')});
        arr.forEach(function(item) {
            dict[item['name']] = item['value'];
        });
        //var data = JSON.stringify(arr);

        $.ajax({
            type: "POST",
            data: dict,
            success: function(data) {
                if (data['response'] == 'ok') {
                    $('.js-send-msg-error span').empty();
                    $('.js-send-msg-success span').text('Сообщение успешно отправлено!');
                    setTimeout(function () {
                        $('#js-msg').css('display', 'none')
                    }, 2500);
                } else {
                    var errors = $.parseJSON(data['errors']);
                    $.each(errors, function(key, value) {
                        $('#js-msg .' + key + ' .errors').text(value[0]['message']);
                    })
                }
            },
            error: function() {
                console.log('ERROR');
            }
        });
    }
};