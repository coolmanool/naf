var getCookie = function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
};

var csrfSafeMethod = function csrfSafeMethod(method) {
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
};

$(function() {
    $('.js-btn-photos-delete').on('click', function() {
        confirmDelete($('.photo .check input:checked'))
    });
    $('.js-btn-projects-delete').on('click', function() {
        confirmDelete($('.project .check input:checked'))
    });

    $('.js-make-main').on('click', ajaxMakePhotoMain);
});

var getIds = function getIds(objs) {
    var objectIds = '';
    objs.each(function() {
       objectIds += $(this).val() + ',';
    });
    return objectIds.slice(0, -1);
};

var confirmDelete = function confirmDelete(objs) {
    var ids = getIds(objs);
    if (ids != '') {
        if (confirm('Please confirm delete')) {
            ajaxDelete(ids);
        }
    }
};

var ajaxDelete = function ajaxDelete(ids) {
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        }
    });

    $.ajax({
        type: "POST",
        data: {obj_ids: ids},
        success: function() {
            window.location.reload();
        },
        error: function() {
            console.log('ERROR');
        }
    });
};

var ajaxMakePhotoMain = function ajaxMakePhotoMain() {
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        }
    });

    $.ajax({
        type: "POST",
        data: {photo_new_id: $(this).attr('data-photo-id'),
            photo_old_id: $('.photo[data-photo-main="True"]').attr('data-photo-id')},
        success: function(){
            window.location.reload();
        },
        error: function(){
            console.log('ERROR');
        }
    });
};