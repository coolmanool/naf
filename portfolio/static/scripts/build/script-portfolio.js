var getCookie = function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
};

var csrfSafeMethod = function csrfSafeMethod(method) {
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
};

var hamburgerClick = function hamburgerClick() {
    var $this = $(this),
        $menuHamburger = $('.js-menu-hamburger'),
        menuNormalColor = '#3b3b3b',
        menuHoverColor = '#0078c7';
    if ($menuHamburger.css('display') != 'none') {
        $menuHamburger.css('display', 'none');
        $this.css('color', menuNormalColor);
        if ($(window).scrollTop() == 0) {
            $('.js-header-opacity').css({'display': 'block', 'opacity': '1'});
            $('.js-header').css('display', 'none');
        }
    } else {
        $menuHamburger.css('display', 'block');
        $this.css('color', menuHoverColor);
    }
};

var hamburgerOpacityClick = function hamburgerOpacityClick() {
    $('.js-header-opacity').css('display', 'none');
    $('.js-header').css({'display': 'block', 'opacity': '1'});
    $('.js-menu-hamburger').css('display', 'block');
    $('.js-hamburger').css('color', '#0078c7');
    //$('.js-hamburger-opacity').css('color', menuNormalColor);
};
$(function() {
    var $window = $(window),
        lastScroll = 0,
        $lastScrollItem = null,
        lastScrollItemAttr = null,
        scrollItems = $("[data-menu-dest^='js']");

    checkWinWidth();

    $window.on('resize', checkWinWidth);
    $window.on('scroll', function() {
        var tmp = scrollMenu(lastScroll, $lastScrollItem, lastScrollItemAttr, scrollItems);
        lastScroll = tmp[0];
        $lastScrollItem = tmp[1];
        lastScrollItemAttr = tmp[2];
    });

    $('.js-modal .js-close').on('click', modalClose);
    $('.js-modal-fade').on('click', modalCloseFromFade);
    $('.modal .content').on('click', stopModalPropagation);
    $('.js-modal-fade').on('mouseover', modalCloseHover);
    $('.modal .content').on('mouseover', stopModalPropagation);
    $('.js-modal-fade').on('mouseout', modalCloseUnhover);
    $('.modal .content').on('mouseout', stopModalPropagation);
    $('[data-modal-src^="js-"]').on('click', modalOpen);

    $('[data-msg^="js-"]').on('click', modalMsgOpen);

    $('.js-hamburger').on('click', hamburgerClick);
    $('.js-hamburger-opacity').on('click', hamburgerOpacityClick);

    $('.js-move-to-contacts').on('click', moveToContacts);

    $('.js-project').on('click', modalProjectOpen);

    $("[data-menu-src^='js-']").on('click', scroll);

    $('.js-send-msg').on('click', sendMsg);

    if ($window.scrollTop() != 0) {
        $(".js-header").css('display', 'block');
        $(".js-header-opacity").css('display', 'none');
    }

    var lastScrollItemArr = checkScroll(scrollItems, $lastScrollItem, lastScrollItemAttr);
    $lastScrollItem = lastScrollItemArr[0];
    lastScrollItemAttr = lastScrollItemArr[1];

    var $player = $('#ytplayer');
    var $loading = $('.loading');

    $player.YTPlayer();

    $player.on('YTPReady', function(e) {
        //
    });

    $player.on('YTPBuffering', function(e) {
       $loading.css('display', 'block');
    });
});
var putDefaultMargins = function putDefaultMargins() {
    $('.project').removeClass('project-second').each(function(index) {
        if (index % 3 == 1) {
            $(this).addClass('project-central');
        }
    });
};

var putHalfMargins = function putHalfMargins() {
    $('.project').removeClass('project-central').each(function(index) {
        if (index % 2 == 1) {
            $(this).addClass('project-second');
        }
    });
};

var removeMargins = function removeMargins() {
    $('.project').removeClass('project-central').removeClass('project-second');
};

var checkWinWidth = function checkWinWidth() {
    var winWidth = $(window).width();
    if (winWidth >= 950) {
        putDefaultMargins();
    } else if(winWidth > 600) {
        putHalfMargins();
    } else {
        removeMargins();
    }
};
var modalClose = function modalClose() {
    $(this).closest('.js-modal').css('display', 'none');
};

var modalCloseFromFade = function modalCloseFromFade(event) {
    $(event.target).closest('.js-modal').css('display', 'none');
};

var modalCloseHover = function modalCloseHover(event) {
    console.log($(event.target).closest('.js-close i'));
    $(event.target).find('.js-close i').css('color', 'white');
};

var modalCloseUnhover = function modalCloseUnhover(event) {
    $(event.target).find('.js-close i').css('color', '#d0d0d2');
};

var stopModalPropagation = function stopModalPropagation(e) {
    e.stopPropagation();
};

var modalOpen = function modalOpen() {
    var src = $(this).attr('data-modal-src');
    $("[data-modal-dest=" + src + "]").css('display', 'block');
};

var modalMsgOpen = function modalMsgOpen() {
    var $msgModal = $('#js-msg'),
        $btn = $msgModal.find('.button'),
        $txtarea = $msgModal.find('.theme input'),
        dataMsg = $(this).attr('data-msg');
    if (dataMsg == 'js-coop') {
        $txtarea.val('Предложение о сотрудничестве');
        $btn.attr('data-msg', 'coop');
    } else if (dataMsg == 'js-sales') {
        $txtarea.val('Предложение по продажам');
        $btn.attr('data-msg', 'sales');
    } else {
        return
    }
    $('.js-send-msg-success span').empty();
    $('.js-send-msg-error span').empty();
    $('.errors').empty();
    $msgModal.css('display', 'block');
};
var moveToContacts = function moveToContacts() {
    var headerHeight = $(".header-normal").height(),
        $contactTitle = $(".contact__title");
    $(this).closest('.modal').css('display', 'none');
    $("html:not(:animated),body:not(:animated)").animate({scrollTop: $("[data-menu-dest='js-contacts']").offset().top - headerHeight}, 800);
    $contactTitle.css("color", "#87cefa");
    setTimeout(function() {
        $contactTitle.css("color", "white");
    }, 2000)
};
var modalProjectOpen = function modalProjectOpen() {
    var src = $(this).attr('data-project');

    $.ajax({
        type: "GET",
        url: '/ajax-project/',
        data: {pk: src},
        success: function(data) {
            var $modalProject = $('#js-modal-project'),
                title = $modalProject.find('.title span'),
                text = $modalProject.find('.text span'),
                $filmroll = $('#filmroll');
            title.text(data['title']);
            text.text(data['text']);

            if ($filmroll.attr('data-is-slicked') === '1') {
                $filmroll.slick('unslick');
            }

            $filmroll.empty();
            $.each(data['videos'], function(index, item) {
                var divElem = $(document.createElement('div')),
                    iframeElem = $(document.createElement('iframe')).attr({'src': item, 'frameborder':'0'});
                $filmroll.append(divElem.append(iframeElem));
            });
            $.each(data['photos'], function(index, item) {
                var divElem = $(document.createElement('div')),
                    //hrefElem = $(document.createElement('a')),
                    imgElem = $(document.createElement('img')).attr('src', item);
                $filmroll.append(divElem.append(imgElem));
            });
            $modalProject.css('display', 'block');

            $filmroll.slick({
                centerMode: true,
                dots: true,
                infinite: true,
                nextArrow: '<button type="button" class="slick-next"><i class="material-icons">chevron_right</i></button>',
                prevArrow: '<button type="button" class="slick-prev"><i class="material-icons">chevron_left</i></button>',
                speed: 300,
                slidesToShow: 1,
                variableWidth: true
            });
            $filmroll.attr('data-is-slicked', '1')
        },
        error: function() {
            console.log('ERROR');
        }
    });
};
var scroll = function scroll() {
    var elementClick = $(this).attr("data-menu-src"),
        destination = $("[data-menu-dest="+elementClick+"]").offset().top,
        headerHeight = $(".header-normal").height();
    $("html:not(:animated),body:not(:animated)").animate({scrollTop: destination - headerHeight}, 800);
};
var checkScroll = function checkScroll(scrollItems, $lastScrollItem, lastScrollItemAttr) {
    var cur = scrollItems.map(function() {
            if ($(this).offset().top <= $(window).scrollTop() + 76) {
                return this;
            }
        }),
        $cur = $(cur[cur.length - 1]),
        curAttr = $cur.attr('data-menu-dest'),
        menuNormalColor = '#3b3b3b',
        menuHoverColor = '#0078c7';

    if (lastScrollItemAttr != curAttr) {
        if ($lastScrollItem) {
            $("[data-menu-src="+lastScrollItemAttr+"]").css('color', menuNormalColor);
        }
        $("[data-menu-src="+curAttr+"]").css('color', menuHoverColor);
        $lastScrollItem = $cur;
        lastScrollItemAttr = curAttr;
    }

    if  ($(window).scrollTop() == $(document).height() - $(window).height()) {
        if ($lastScrollItem) {
            $("[data-menu-src="+lastScrollItemAttr+"]").css('color', menuNormalColor);
        }
        $cur = $("[data-menu-src='js-contacts']");
        $cur.css('color', menuHoverColor);
        $lastScrollItem = $cur;
        lastScrollItemAttr = 'js-contacts';
    }

    return [$lastScrollItem, lastScrollItemAttr]
};

var scrollMenu = function scrollMenu(lastScroll, $lastScrollItem, lastScrollItemAttr, scrollItems) {
    var $this = $(this),
        animateTime = 600,
        easing = "linear",
        $header = $(".js-header"),
        $headerOpacity = $(".js-header-opacity");

    if (lastScroll == 0) {
        $header.css('display', 'block');
        $header.animate(
            {'opacity': '1'},
            animateTime,
            easing
        );

        $headerOpacity.animate(
            {'opacity': '0'},
            animateTime,
            easing,
            function () { $(this).css('display', 'none'); }
        );

        lastScroll = $this.scrollTop();

    } else if (($this.scrollTop() <= 0) && ($('.js-menu-hamburger').css('display') == 'none')) {
        lastScroll = $this.scrollTop();
        $header.animate(
            {'opacity': '0'},
            animateTime,
            easing,
            function() { $(this).css('display', 'none') }
        );

        $headerOpacity.css('display', 'block');
        $headerOpacity.animate(
            {'opacity': '1'},
            animateTime,
            easing
        );
    }

    var lastScrollItemArr = checkScroll(scrollItems, $lastScrollItem, lastScrollItemAttr);
    return [lastScroll, lastScrollItemArr[0], lastScrollItemArr[1]]
};

var checkMsgFields = function checkMsgFields($btn) {
    var form = $btn.closest('form'),
        inputFields = form.find('input'),
        flag = true;
    inputFields.push(form.find('textarea')[0]);
    inputFields.each(function() {
        if ($(this).val() === '') {
            $('.js-send-msg-error span').text('Все поля формы должны быть заполнены!');
            flag = false;
        }
    });
    return flag
};

var sendMsg = function sendMsg() {
    if (checkMsgFields($(this))) {
        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
            }
        });

        var arr = $(this).closest('form').serializeArray(),
            dict = {};
        arr.push({name: 'msg_type', value: $(this).attr('data-msg')});
        arr.forEach(function(item) {
            dict[item['name']] = item['value'];
        });
        //var data = JSON.stringify(arr);

        $.ajax({
            type: "POST",
            data: dict,
            success: function(data) {
                if (data['response'] == 'ok') {
                    $('.js-send-msg-error span').empty();
                    $('.js-send-msg-success span').text('Сообщение успешно отправлено!');
                    setTimeout(function () {
                        $('#js-msg').css('display', 'none')
                    }, 2500);
                } else {
                    var errors = $.parseJSON(data['errors']);
                    $.each(errors, function(key, value) {
                        $('#js-msg .' + key + ' .errors').text(value[0]['message']);
                    })
                }
            },
            error: function() {
                console.log('ERROR');
            }
        });
    }
};
$(function(){
    var $sliderbox =  $('.slider-box'),
        $slides = $('.slide'),
        $thumbs = $('.slider__thumbs li'),
        slideswidth = $slides.outerWidth(true);

    function getMeTransFromIndex(that) {
        return $(that).index() === 0 ? 'left' : 'right';
    }

    window.makeMoveSlider = function makeMoveSlider(data) {
        var dfd = $.Deferred(),
            params = null;

        if (data === 'left') {
            params = {
                direction: '+=',
                callback: function(){
                    dfd.resolve();
                    $('.slider-box').addClass('_show');
                },
                beforeAnimate: function() {
                    $slides.last().prependTo($sliderbox);
                    $sliderbox.css('left', -734*2);
                    $slides = $('.slide');
                }
            };
        } else {
            params = {
                direction: '-=',
                callback: function(){
                    $slides.first().appendTo($sliderbox);
                    $slides = $('.slide');
                    $sliderbox.css('left',-693);
                    dfd.resolve();
                    $('.slider-box').addClass('_show');
                }
            };
        }

        sliderspin(params);
        return dfd.promise();
    };

    function makeSwitchBubbles(data) {
        var curPos = $thumbs.index($thumbs.filter('._act')),
            iterator = (function() {
                return data === 'left' ? -1 : +1;
            }()),
            nextIndex = (function() {
                var value = null;

                if (iterator > 0 && curPos < $thumbs.length - 1 || iterator < 0 && curPos > 0) {
                    value = curPos + iterator;
                } else if(iterator > 0) {
                    value = 0;
                } else if (iterator < 0) {
                    value = $thumbs.length - 1;
                }

                return value;
            }());

        $thumbs.removeClass('_act').eq(nextIndex).addClass('_act');
    }

    window.decoratorMoveSlider = function decoratorMoveSlider(data) {
        var direction = data < 0 ? 'left' : 'right',
            counter = Math.abs(data);

        $slides.trigger('mouseleave');
        (function loop(counter) {
            makeSwitchBubbles(direction);
            makeMoveSlider(direction).done(function() {
                if (counter -= 1 >= 0) {
                    loop(counter)
                } else {
                    $slides.trigger('mouseenter');
                }
            });
        }(counter));

    };

    function sliderspin(params){
        if ('beforeAnimate' in params) {
            params.beforeAnimate();
        }
        $('.slider-box').removeClass('_show');
        $sliderbox.animate({left: params.direction + slideswidth}, 1000, 'linear', params.callback)
    }

    $thumbs.click(function(){
        if ($sliderbox.is(':animated')) return;

        var curBubbleIndex = $(this).index(),
            actBubbleIndex = $thumbs.index($thumbs.filter('._act'));

        if (actBubbleIndex === curBubbleIndex) {
            return false;
        }

        $slides.trigger('mouseleave');
        decoratorMoveSlider(curBubbleIndex - actBubbleIndex);
    });

    $slides.on({
        click: function(){
            if($sliderbox.is(':animated')){
                return false
            }
            $slides.trigger('mouseleave');

            clearInterval(interval);

            var data = getMeTransFromIndex(this);
            makeMoveSlider(data).done(function() {
                $slides.trigger('mouseenter');
                interval =  setInterval(function() {
                        makeMoveSlider();
                        makeSwitchBubbles();
                    }, 5500
                );
            });
            makeSwitchBubbles(data);
        },
        mouseenter: function() {
            var that = this;

            setTimeout(function() {
                if ($sliderbox.is(':animated')) return false;

                if (/0|2/.test($(that).index())) {
                    $(that).addClass('_hover')
                }
            }, 0)
        },
        mouseleave: function() {
            $(this).removeClass('_hover')
        }
    });
    var interval = setInterval(function() {
            makeMoveSlider();
            makeSwitchBubbles()

        }, 100000
    );

    $thumbs.on({
        'mouseenter':function(){
            clearInterval(interval);
        },
        'mouseleave':function(){
            interval =  setInterval(function() {
                    makeMoveSlider();
                    makeSwitchBubbles();
                }, 5500
            );
        }
    })
})
