import os

from django.db import models

import json
from datetime import date


def get_file_path(instance, filename):
    return os.path.join('projects', 'project-{}'.format(instance.project.id), filename)


class Project(models.Model):
    title = models.CharField(max_length=255)
    text = models.TextField()
    title_eng = models.CharField(max_length=255, default='', blank=True)
    text_eng = models.TextField(default='', blank=True)
    videos = models.CharField(max_length=1000, default='[]', blank=True)
    date = models.DateField(default=date.today(), blank=False)

    def save(self, *args, **kwargs):
        if self.videos:
            self.videos = json.dumps([self.videos.replace('watch?v=', 'embed/')])
        else:
            self.videos = '[]'
        super(Project, self).save(*args, **kwargs)


class Photo(models.Model):
    photo = models.FileField(upload_to=get_file_path)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    main_photo_flag = models.BooleanField(default=False)


# class ProjectEng(models.Model):
#     title = models.CharField(max_length=255)
#     text = models.TextField()
#     project = models.OneToOneField(Project, on_delete=models.CASCADE)
