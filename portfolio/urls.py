from django.conf import settings
from django.conf.urls import url, i18n
from django.views import static
from . import views

urlpatterns = [
    # tech
    url(r'^i18n/', i18n.set_language, name='set_language'),
    url(r'^media/(?P<path>.*)$', static.serve, {'document_root': settings.MEDIA_ROOT}),

    # main
    url(r'^draft$', views.draft, name='draft'),
    url(r'^$', views.portfolio, name='portfolio'),

    # admin
    url(r'^create-project$', views.create_project, name='create_project'),
    url(r'^edit-project/(?P<pk>[0-9]+)$', views.edit_project, name='edit_project'),
    url(r'^projects$', views.projects, name='projects'),

    # ajax
    url(r'^ajax-project/$', views.ajax_project, name='ajax_project'),

    # seo
    url(r'^robots.txt$', views.robots, name='robots'),
    url(r'^sitemap.xml$', views.sitemap, name='sitemap'),

    #yamail
    url(r'^yamail-17080809050285225.html$', views.yamail, name='yamail'),
]
